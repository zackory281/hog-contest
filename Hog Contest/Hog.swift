//
//  Hog.swift
//  Hog Contest
//
//  Created by Zackory Cramer on 6/30/18.
//  Copyright © 2018 Zackory Cui. All rights reserved.
//

import Foundation
typealias Scores = (score1:Int, score2:Int)
typealias OUT_PROB = [Int:Double]
typealias ROLLS_OUT_PROB = [Int:OUT_PROB]
typealias SCORE_VALUE_MAP = Dictionary<String, Double>
typealias SCORE_ROLL_MAP = [String:Int]
let MAXROLLS:Int = 10
let GOAL:Int = 100

class Hog {
    static var is_finding_guarrentee:Bool = true
    // dynamic programming
    static func getWinRateForScores(_ scores:Scores, player_turn:Bool = true, turn:Int = -1)->Double{
        if player_turn {
            if scores.score1 >= GOAL {return PLAYER_WIN_VALUE}
            if scores.score2 >= GOAL {return OPPONE_WIN_VALUE}
            if let value = getValueFromScores(scores, map: Hog.scores_value_map) {return value}
            var value_per_roll:[Int:Double] = [:]
            for roll in 0...MAXROLLS{
                value_per_roll[roll] = getExpectedValueForRoll(scores, roll:roll, player_turn: player_turn)
            }
            let bestKeyPair = value_per_roll.max{return $0.value<$1.value}!
            setRollProbForMap(scores: scores, winProb: bestKeyPair.value, bestRoll: bestKeyPair.key, valueMap: &Hog.scores_value_map, rollMap: &Hog.scores_roll_map)
            return bestKeyPair.value
        }else{
            if scores.score1 >= GOAL {return getWinRateForScores(reverseScores(scores), player_turn:true)}
            if scores.score2 >= GOAL {return getWinRateForScores(reverseScores(scores), player_turn:true)}
            if let value = getValueFromScores(scores, map: Hog.scores_oppo_value_map) {return value}
            if is_finding_guarrentee {
                //test
                var least_prob:Double = 1
                var least_prob_roll:Int = 0
                for roll in 0...10{
                    let winRate:Double = getExpectedValueForRoll(scores, roll: roll, player_turn: player_turn)
                    if winRate < least_prob{
                        least_prob = winRate
                        least_prob_roll = roll
                    }
                }
                //test
                
                setRollProbForMap(scores: scores, winProb: least_prob, bestRoll: least_prob_roll, valueMap: &Hog.scores_oppo_value_map, rollMap: &Hog.scores_oppo_roll_map)
                return least_prob
            }else{
                var least_prob:Double = 0
                var least_prob_roll:Int = 0
                let roll = oppo_strategy(scores)
                let winRate:Double = getExpectedValueForRoll(scores, roll: roll, player_turn: player_turn)
                least_prob = winRate
                least_prob_roll = roll
                //roll = ge
                setRollProbForMap(scores: scores, winProb: least_prob, bestRoll: least_prob_roll, valueMap: &Hog.scores_oppo_value_map, rollMap: &Hog.scores_oppo_roll_map)
                return least_prob
            }
        }
    }
    
    static func getExpectedValueForRoll(_ scores:Scores, roll:Int, player_turn:Bool) -> Double{
        var value:Double = 0
        let outcome_map:[Int:Double] = roll == 0 ? [0:1.0] : rolls_out_prob[roll]!
        for (outcome, prob) in outcome_map{//(99, 92) then rolls 3
            let new_scores = rollFilter(scores, roll_outcome: outcome)//(92, 102)
            let toPassScore = reverseScores(new_scores)
            value += getWinRateForScores(toPassScore, player_turn:!player_turn) * prob
        }
        return value
    }
    
    static func oppo_strategy(_ scores:Scores)->Int{
        return 6//Hog.getRollFromScores(scores, map: Hog.scores_oppo_roll_map) ?? 6
    }
    // rules
    static func rollFilter(_ scores:Scores, roll_outcome:Int)->Scores{
        let after_roll_scores:Scores
        if roll_outcome == 0{
            after_roll_scores = rollZeroFilter(scores)
        }else{
            after_roll_scores = rollNormalFilter(scores, outcome: roll_outcome)
        }
        return swineSwapFilter(after_roll_scores)
    }
    static func rollZeroFilter(_ scores:Scores)->Scores{
        let score2 = scores.score2
        let gain  = ((score2 % 10) * (score2 / 10)) % 10 + 1
        return ((scores.score1 + gain, scores.score2))
    }
    static func rollNormalFilter(_ scores:Scores, outcome:Int)->Scores{
        return (scores.score1 + outcome, scores.score2)
    }
    static func swineSwapFilter(_ scores:Scores)->Scores{
        if scores.score1 % 10 == scores.score2 / 10 {
            return reverseScores(scores)
        }
        return scores
    }
    static var rolls_out_prob:ROLLS_OUT_PROB = [:]
    static var scores_value_map:SCORE_VALUE_MAP = [:]
    static var scores_roll_map:SCORE_ROLL_MAP = [:]
    static var scores_oppo_value_map:SCORE_VALUE_MAP = [:]
    static var scores_oppo_roll_map:SCORE_ROLL_MAP = [:]
}

let PLAYER_WIN_VALUE:Double = 1
let OPPONE_WIN_VALUE:Double = 0

extension Hog {
    static func getValueFromScores(_ scores:Scores, map:SCORE_VALUE_MAP) -> Double?{
        if let k = map[getScoreString(scores)] {return k}
        //else if let k = map[getScoreString(reverseScores(scores))] {return 1 - k}
        return nil
    }
    static func getRollFromScores(_ scores:Scores, map:SCORE_ROLL_MAP) -> Int?{
        if let k = map[getScoreString(scores)] {return k}
        //else if let k = map[getScoreString(reverseScores(scores))] {return k}
        return nil
    }
    static func setRollProbForMap(scores:Scores, winProb:Double?, bestRoll:Int?, valueMap:inout SCORE_VALUE_MAP, rollMap:inout SCORE_ROLL_MAP){
        if let prob = winProb{
            valueMap[getScoreString(scores)] = prob
        }
        if let roll = bestRoll{
            rollMap[getScoreString(scores)] = roll
        }
    }
    static func getScoreString(_ scores:Scores) -> String{
        return "\(scores.score1):\(scores.score2)"
    }
    static func reverseScores(_ scores:Scores)->Scores{
        return (scores.score2, scores.score1)
    }
    static func trim(_ prob:Double)->Double{
        return min(max(prob, 0), 1)
    }
}
