//
//  Calculator.swift
//  Hog Contest
//
//  Created by Zackory Cramer on 6/30/18.
//  Copyright © 2018 Zackory Cui. All rights reserved.
//

import Foundation

class Calculatorr {
    var rolls_out_prob:ROLLS_OUT_PROB = [:]
    
    func calculate(){
        for rolls in 1...10{
            var out_prob:OUT_PROB = [:]
            for _ in 0...samples{
                var out = 0
                for _ in 1...rolls{
                    let rand = arc4random_uniform(6) + 1
                    if rand == 1 {
                        out = 1
                        break
                    }
                    out += Int(rand)
                }
                out_prob[out] = (out_prob[out] ?? 0) + 1
            }
            for out_key in out_prob.keys{
                out_prob[out_key] = out_prob[out_key]! / Double(samples)
            }
            rolls_out_prob[rolls] = out_prob
        }
    }
    
    let samples:UInt64 = 99999
}


class Calculator {
    var key:String
    init(_ samples:UInt64=99999,key:String="map5") {
        self.samples = samples
        self.key = key
    }
    var encoder:JSONEncoder = JSONEncoder()
    var decoder:JSONDecoder = JSONDecoder()
    var percentage:Double = 0
    var roll_exp:ROLLS_OUT_PROB = [:]
    func getAveragesForDie(num_die:Int) -> [Int:Double] {
        var dict:Dictionary<Int,Int64> = [:]
        for i in 0...samples{
            var sub:Int = 0
            for _ in 1...num_die{
                let rand = arc4random_uniform(6)+1
                if rand == 1{
                    sub = 1
                    break
                }
                sub += Int(rand)
            }
            dict[sub] = (dict[sub] ?? 0) + 1
            if i%(samples/100) == 0 {
                percentage += 1
                print("done \(percentage/1000)")
            }
        }
        let ans = dict.mapValues{int64 in
            return Double(int64) / Double(samples)
        }
        return ans
    }
    
    func calculateRollExpMap(){
        for numDie in Int(1)...Int(10){
            roll_exp[numDie] = getAveragesForDie(num_die: numDie)
        }
        roll_exp[0] = [0:1.0]
        encodeResults()
    }
    func encodeResults(){
        do{
            try UserDefaults.standard.set(encoder.encode(roll_exp) , forKey: key)
        }catch {
            print("encoding errored \(error)")
        }
    }
    func decodeResults(){
        do{
            try roll_exp = decoder.decode([Int:[Int:Double]].self, from: UserDefaults.standard.value(forKey: key) as! Data)
        }catch {
            print("decoding errored \(error)")
        }
    }
    
    var samples:UInt64 = 100000
}
