//
//  main.swift
//  Hog Contest
//
//  Created by Zackory Cramer on 6/30/18.
//  Copyright © 2018 Zackory Cui. All rights reserved.
//

import Foundation

class Main{
    let cal = Calculator(key:"map")
    let hog = Hog()
    func main(){
        //cal.calculateRollExpMap()
        cal.decodeResults()
        Hog.rolls_out_prob = cal.roll_exp
        let scores = (0, 0)
        let _ = Hog.getWinRateForScores(scores)
//        Hog.scores_oppo_roll_map = Hog.scores_roll_map
//        Hog.scores_oppo_value_map = [:]
//        Hog.scores_roll_map = [:]
//        Hog.scores_value_map = [:]
        let _ = Hog.getWinRateForScores(scores)
        let ro = Hog.getRollFromScores(scores, map: Hog.scores_roll_map)!
        print("at \(scores), I would roll \(ro) die win \(Hog.getValueFromScores(scores, map: Hog.scores_value_map)!)%")
        printOut()
    }
    func display(){
        for s1 in 0...99{
            for s2 in 0...99{
                let curScore = (s1, s2)
                let _ = Hog.getWinRateForScores(curScore)
            }
        }
        printTwo()
    }
    func printTwo(){
        var str = "[\n"
        for s1 in 0...99{
            for s2 in 0...99{
                let curScore = (s1, s2)
                if let roll = Hog.getRollFromScores(curScore, map: Hog.scores_roll_map){
                    str.append("\"\(Hog.getScoreString(curScore))\":\(roll),")
                } else{
                    str.append("6,")
                }
            }
        }
        str.append("]\n")
        print(str)
    }
    func printOut(){
        var str = "[\n"
        for s1 in 0...99{
            str.append("[")
            for s2 in 0...99{
                let curScore = (s1, s2)
                if let roll = Hog.getRollFromScores(curScore, map: Hog.scores_roll_map){
                    str.append("\(roll),")
                } else{
                    str.append("6,")
                }
            }
            str.append("],\n")
        }
        str.append("]\n")
        print(str)
    }
}

let main = Main()
main.main()
